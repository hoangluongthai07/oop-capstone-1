# `PhoneTariff.App` Cước Điện thoại

## Giới thiệu
Bạn làm việc tại một công ty điện thoại. Công ty này cung cấp nhiều loại cước điện thoại khác nhau. Tuy nhiên, bạn có thể nhóm chúng thành ba loại cước cơ bản khác nhau (lưu ý rằng chúng ta bỏ qua cuộc gọi điện thoại trong ví dụ đơn giản này và tập trung chỉ vào việc sử dụng dữ liệu):

1. Phí cố định(`Flat Fee`): Khách hàng trả một số tiền cố định mỗi tháng và sau đó không trả thêm bất cứ chi phí nào. Nó bao gồm tất cả cuộc gọi và việc truyền dữ liệu.

2. Thanh toán theo sử dụng(`Pay As You Go`): Khách hàng không trả bất kỳ phí cơ bản nào. Họ chỉ trả cho mỗi megabyte dữ liệu truyền. Nghĩa là không sử dụng sẽ không bị tính phí.

3. Starter(`Mixed`): Khách hàng trả một  phần phí cơ bản. Phí này sẽ cho khách hàng một lượng megabyte cụ thể ví dụ 10MB. Nếu khách hàng sử dụng 11MB(megabyte) thì họ phải trả phí cho 1MB cho dữ liệu vượt quá.

Bạn phải viết một chương trình tính toán số tiền phải trả. Người dùng nhập các thông số của cước của mình (ví dụ: loại cước, megabyte đã bao gồm, phí cơ bản, v.v.). 

## Yêu cầu
Bạn phải viết một chương trình mà người dùng có thể nhập các thông số của cước của mình (ví dụ: loại cước, megabyte đã bao gồm, phí cơ bản, v.v.) cũng như sử dụng dữ liệu hàng tháng của cô ấy. Sau đó, chương trình tính toán tổng chi phí cho tháng.

1. Tạo project kiểu **Library** có tên là `PhoneTariff.Logic`
  - project này dùng để chứa logic, tính toán

2. Tạo project kiểu **Console** `PhoneTariff.App.` Nó chứa code để tương tác của người dùng. Ví dụ như đọc dữ liệu từ bàn phím khởi tạo đối tượng, gọi đến các class trong project `PhoneTariff.Logic` nơi chứa logic tính toán và hiển thị dữ liệu trả về từ những hàm này

![Project structure](project-structure.png)

3. Trong project `PhoneTariff.Logic`, triển khai các class sau:

![tariff-classdiagram](tariff-classdiagram.png)

### Các kết quả mẫu:
1. kiểu [F]lat fee
```text
Loại cước nào? [F]lat fee, [M]ixed, [P]ay as you go: f
Phí hàng tháng: 220000
Bạn phải trả 220,000 (VND)
```

2. kiểu [M]ixed
```text
Loại cước nào? [F]lat fee, [M]ixed, [P]ay as you go: m
Phí hàng tháng: 120000
Megabyte đã bao gồm: 150
Giá mỗi megabyte: 5000
Bạn đã sử dụng bao nhiêu megabyte? 250
Bạn phải trả 120,000 + (250 - 150) * 5,000 = 720,000 (VND)
```

3. kiểu [P]ay as you go
```text
Loại cước nào? [F]lat fee, [M]ixed, [P]ay as you go: p
Giá mỗi megabyte: 7000
Bạn đã sử dụng bao nhiêu megabyte? 200
Bạn phải trả 200 * 7,000 = 1,400,000 (VND)
```

Hãy kiểm tra lại các trường hợp xem code đã hoạt động đúng như mong đợi chưa

## Làm mới chương trình

Hãy thực thi yêu cầu này sau khi chương trình đã chạy được như mong đợi

### Thay đổi Cấu trúc class
Điều chỉnh cấu trúc class như sau:
2 class Mixed & PayAsYouGo đều có thuộc tính `MonthlyFee` để tránh lặp code chúng ta sẽ thay đổi cấu trúc một chút

![Alt text](stariff-restructure.png)

**Lưu ý**: sau khi cấu trúc class thay đổi thì chương trình vẫn hoạt động một cách bình thường.

### Import
Bạn có một [MOCK_DATA.csv](MOCK_DATA.csv) file này chứa dữ liệu về sử dụng data trong 1 tháng(12/2023). File chứa thông tin Ngày và Số lượng megabytes sử dụng, lưu ý 1 ngày có thể có nhiều dòng dữ liệu. bên dưới là ví dụ 1 vài dòng dữ liệu.

```csv
Date,MegabytesUsage
12/18/2023,39
12/7/2023,51
12/27/2023,27
12/29/2023,46
12/9/2023,38
12/1/2023,27
...
```
**Nhiệm vụ của bạn** : đọc dữ liệu từ file và tính toán ra tổng chi phí phải trả. 

1. Việc phân tích cần được thực hiện trong project `PhoneTariff.Logic`
2. `Importer` là một **static class**, hàm `ImportUsage` sẽ đọc dữ liệu từ file `MOCK_DATA.csv` trả về mảng các đối tượng có kiểu `Usage`
3. class `Usage` chứa các thuộc tính (Date, MegabytesUsage) để chứa dữ liệu.

Sau khi có được mảng các đối tượng Usage thì bạn có thể tính ra số megabytes khách hàng sử dụng mỗi tháng dựa vào con số này thể tính tiền cho: FlatFee, Mixed & Pay As You Go
Sample outputs:

4. hãy tạo project tên là `PhoneTariff.Import` dùng để viết giao diện cho người dùng nhập vào và logic gần giống như `PhoneTariff.App` chỉ có điều lúc này chương trình sẽ không hỏi câu `Bạn đã sử dụng bao nhiêu megabyte?` dữ liệu này được lấy từ tổng các phần tử trong mảng `Usage` trả về từ hàm `ImportUsage` trong class `Importer`

**cấu trúc mới sẽ như sau**
![project-re-structure](project-re-structure.png)

### Các kết quả mẫu:
1. kiểu [F]lat fee
```text
Loại cước nào? [F]lat fee, [M]ixed, [P]ay as you go: f
Phí hàng tháng: 220000
Bạn phải trả 220,000 (VND)
```

2. kiểu [M]ixed `giả sử tổng số megabyte trong file là 500MB`
```text
Loại cước nào? [F]lat fee, [M]ixed, [P]ay as you go: m
Phí hàng tháng: 120000
Megabyte đã bao gồm: 150
Giá mỗi megabyte: 5000
Bạn phải trả 120,000 + (500 - 150) * 5,000 = 1,750,120 (VND)
```

3. kiểu [P]ay as you go `giả sử tổng số megabyte trong file là 300MB`
```text
Loại cước nào? [F]lat fee, [M]ixed, [P]ay as you go: p
Giá mỗi megabyte: 7000
Bạn phải trả 300 * 7,000 = 2,100,000 (VND)
```